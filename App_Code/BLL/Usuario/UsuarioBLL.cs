﻿using ApiGestao.Context;
using AppGestao.App_Code.BLL;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiGestao.Model;
using System;
using Microsoft.EntityFrameworkCore;
using AppGestao.Uteis;

namespace ApiGestao.App_Code.BLL
{
    public class UsuarioBLL
    {
        private readonly ApplicationDbContext _context;
        private MapearClasse map = new MapearClasse();
        public UsuarioBLL(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<UsuarioView> LoginUsuarioAsync(Usuario usr)
        {
             
             var usuario = await _context.Usuario
             .Include(u => u.Perfil)
             .Include(u => u.Pessoa)
             .FirstOrDefaultAsync(e => e.Pessoa.contato.Email.Trim() == usr.NomeUsr.Trim() && e.Senha.Trim() == usr.Senha.Trim() && e.FlagAtivo == true);

            UsuarioView usuarioView = new UsuarioView();


            if (usuario != null)
            {
                usuarioView.CodValidacao = usuario.CodValidacao;
                usuarioView.Id = usuario.Id;
                usuarioView.NomeUsr = usuario.NomeUsr;
                usuarioView.Senha = usuario.Senha;
                usuarioView.FlagAtivo = usuario.FlagAtivo;
                usuarioView.PessoaId = usuario.PessoaId;
                usuarioView.Pessoa = usuario.Pessoa;
                usuarioView.PerfilId = usuario.PerfilId;
                usuarioView.Perfil = usuario.Perfil;
            }

      

            return usuarioView;
        }






        public bool CpfExists(Usuario usr)
        {
            return _context.Pessoa.Any(e => e.Cpf.Trim() == usr.Pessoa.Cpf.Trim());
        }


        public bool EmailExists(Usuario usr)
        {
            return _context.Contato.Any(e => e.Email.Trim() == usr.Pessoa.contato.Email.Trim());
        }

        public bool UsuarioExists(Usuario usr)
        {
            return _context.Usuario.Any(e => e.NomeUsr.Trim() == usr.NomeUsr.Trim());
        }

        public int GerarCodigoValidacaoUsuario()
        {
            Random randNum = new Random();
            return randNum.Next(100000, 999999); 
        }

        public Usuario CadastroUsuario(Usuario usr)
        {
            usr.FlagAtivo = false;
            usr.NomeUsr = usr.Pessoa.contato.Email;
            usr.CodValidacao = GerarCodigoValidacaoUsuario();
            usr.Pessoa.DtCadastro = DateTime.Now;
            return usr;
        }

        public async Task<bool> ValidarCodigo(int id, int codValidacao)
        {
            try
            {
                bool valido = _context.Usuario.Any(e => e.Id == id && e.CodValidacao == codValidacao);

                if (valido)
                {
                    //Busca Usuário por ID
                    var usuario = await _context.Usuario
                                     .FirstOrDefaultAsync(m => m.Id == id && m.FlagAtivo == false);
                  //Habilita o flag de ativação do usuário
                    usuario.FlagAtivo = true;
                    _context.Entry(usuario).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                    return valido;
                }
                return valido;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Perfil InserirPerfil()
        {
            try
            {
                Perfil perfil = new Perfil();
                perfil.Decricao = "Usuário do sistema";
                perfil.FlagIdentPerf = "U";
                return perfil;
            }
            catch (Exception e)
            {
                return null;
                throw new Exception(e.Message);
            }

        }

    }
}

