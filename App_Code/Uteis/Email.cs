﻿using ApiGestao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace AppGestao.App_Code.Email
{
    public class Email
    {
        public void MandarEmail(Usuario usuario)
        {
            SmtpClient client = new SmtpClient("smtp-mail.outlook.com");
            client.Port = 587;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential("lucasnathan2012@hotmail.com", "99253362");
            client.EnableSsl = true;
            client.Credentials = credentials;
            client.TargetName = "smtp-mail.outlook.com";
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("lucasnathan2012@hotmail.com", string.Empty, System.Text.Encoding.UTF8);
                mail.To.Add(new MailAddress(usuario.Pessoa.contato.Email));
                mail.Subject = "Validação de Usuário";
                mail.Body = "Código de acesso ao sistema: <br/> Código:  " + usuario.CodValidacao + "<br/>";
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;


                client.Send(mail);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }
    }
}
