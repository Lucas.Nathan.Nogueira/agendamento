﻿using ApiGestao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppGestao.Uteis
{
    public class MapearClasse
    {
        public UsuarioView MapUsuario(Usuario usr)
        {
            UsuarioView usuarioView = new UsuarioView();
            usuarioView.CodValidacao = usr.CodValidacao;
            usuarioView.Id = usr.Id;
            usuarioView.NomeUsr = usr.NomeUsr;
            usuarioView.Senha = usr.Senha;
            usuarioView.FlagAtivo = usr.FlagAtivo;
            usuarioView.PessoaId = usr.PessoaId;
            usuarioView.Pessoa = usr.Pessoa;
            usuarioView.PerfilId = usr.PerfilId;
            usuarioView.Perfil = usr.Perfil;
            return usuarioView;
    }
    }
}
