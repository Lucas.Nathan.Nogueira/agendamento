import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../login/Component/login/login.component';
import { CadastroComponent } from '../login/Component/Cadastros/cadastro/cadastro.component';

const Routings: Routes = [
  { path: 'Registrar', component: CadastroComponent },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(Routings)
  ],
  exports:[RouterModule]
})
export class LoginRoutingModule { }
