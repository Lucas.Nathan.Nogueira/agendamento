import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './Component/login/login.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { LoginRoutingModule } from '../login-routing/login-routing.module';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';
import { CadastroComponent } from './Component/Cadastros/cadastro/cadastro.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';

@NgModule({
  declarations: [LoginComponent, LoginLayoutComponent, CadastroComponent],
  imports: [
    CommonModule,
    LoginRoutingModule, MatIconModule, MatCardModule, MatTabsModule, MatStepperModule, MatFormFieldModule
  ],
  exports: [],
})
export class LoginModule { }
