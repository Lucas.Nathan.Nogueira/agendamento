import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { LoginLayoutComponent } from '../Login/login/login-layout/login-layout.component';
import { LoginModule } from '../Login/login/login.module';
import { LoginComponent } from '../Login/login/Component/login/login.component';


const AppRoutes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: "Login", component: LoginLayoutComponent,
    children: [
      {
        path: "",
        loadChildren: "../Login/login/login.module#LoginModule", 
         component: LoginComponent,
      }
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forRoot(AppRoutes)
  ],
exports: [RouterModule]
})

export class AppRoutingModule { }
