﻿using ApiGestao.App_Code.BLL;
using ApiGestao.Authentication.Services;
using ApiGestao.Context;
using ApiGestao.Model;
using AppGestao.ViewsModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ApiGestao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UsuarioBLL BLL;

        public LoginController(ApplicationDbContext context)
        {
            _context = context;
            BLL = new UsuarioBLL(_context);
        }

        [HttpPost]
        [Route("/Auth/login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody] Usuario user)
        {
            Response<UsuarioView> response = new Response<UsuarioView>();
            try
            {

                UsuarioView usuario = await BLL.LoginUsuarioAsync(user);
                if (!usuario.FlagAtivo)
                {
                    response.message = "Usuário ou Senha Invalido!";
                    return new { response.message };
                }

                usuario.access_token = TokenService.GenerateToken(user);
                usuario.Senha = "";

                response.data = usuario;
                response.message = "Usuário autentificado com Sucesso!";
                response.success = true;
                return new { response.data };
            }
            catch (Exception e)
            {
                response.errors.Add("Usuário Invalido!");
                response.errors.Add("e.Message!");
                response.message = e.Message;
                response.success = false;
                throw new Exception(e.Message);
            }

         
        }


        [HttpGet]
        [Route("/authenticated")]
        [Authorize]
        public string authenticated()
        {
            return string.Format("Autentificado - {0}", User.Identity.Name);
        }
    }
}