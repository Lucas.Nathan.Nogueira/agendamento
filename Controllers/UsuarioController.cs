﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiGestao.Context;
using ApiGestao.Model;
using Microsoft.AspNetCore.Authorization;
using ApiGestao.App_Code.BLL;
using AppGestao.App_Code.Email;
using AppGestao.ViewsModel;

namespace AppGestao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UsuarioBLL BLL;
        public UsuarioController(ApplicationDbContext context)
        {
            _context = context;
            BLL = new UsuarioBLL(context);
        }

        // GET: api/Usuario
        [HttpGet("PesquisaALL")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Usuario>>> GetUsuario()
        {
            try
            {
                return await _context.Usuario
                           .Include(u => u.Perfil)
                           .Include(u => u.Pessoa)
                             .Include(u => u.Pessoa.contato)
                           .ToListAsync();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        // GET: api/Usuario/5
        [HttpGet("PesquisaId/{id}")]
        [Authorize]
        public async Task<ActionResult<Usuario>> GetUsuario(int id)
        {
            try
            {
                var usuario = await _context.Usuario
              .Include(u => u.Perfil)
              .Include(u => u.Pessoa)
              .FirstOrDefaultAsync(m => m.Id == id);

                if (usuario == null)
                {
                    return NotFound();
                }

                return usuario;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }


        }

        // PUT: api/Usuario/5
        [HttpPut("Editar/{id}")]
        [Authorize]
        public async Task<IActionResult> PutUsuario(int id, UsuarioView usuario)
        {
            if (id != usuario.Id)
            {
                return BadRequest();
            }
            try
            {
                _context.Entry(usuario).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Usuario
        [HttpPost("Cadastrar")]
        public async Task<ActionResult<dynamic>> PostUsuario(Usuario usuario)
        {
       
            try
            {
                Response<UsuarioView> response = new Response<UsuarioView>();
                if (BLL.CpfExists(usuario))
                {
                    response.message = "Cpf já está vinculado a uma conta!";
                    return new { response.message };
                }


                if (BLL.EmailExists(usuario))
                {

                    response.message = "Email já está vinculado a uma usuário!";
                    return new { response.message };
                }
                usuario.Perfil = BLL.InserirPerfil();
                usuario = BLL.CadastroUsuario(usuario);
                _context.Usuario.Add(usuario);
                await _context.SaveChangesAsync();

                Email email = new Email();
                email.MandarEmail(usuario);
                return CreatedAtAction("GetUsuario", new { id = usuario.Id }, usuario);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        // DELETE: api/Usuario/5
        [HttpDelete("Delete/{id}")]
        [Authorize]
        public async Task<ActionResult<Usuario>> DeleteUsuario(int id)
        {
            try
            {
                var usuario = await _context.Usuario
                            .Include(u => u.Perfil)
                            .Include(u => u.Pessoa)
                            .FirstOrDefaultAsync(m => m.Id == id);

                if (usuario == null)
                {
                    return NotFound();
                }

                _context.Usuario.Remove(usuario);
                await _context.SaveChangesAsync();

                return usuario;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }


        [HttpPut("Desabilitar/{id}")]
        [Authorize]
        public async Task<IActionResult> PutDesabilitaUsuario(int id, bool FlagAtivo)
        {
            try
            {
                var usuario = await _context.Usuario
                                      .Include(u => u.Perfil)
                                      .Include(u => u.Pessoa)
                                      .FirstOrDefaultAsync(m => m.Id == id);

                if (id != usuario.Id)
                {
                    return BadRequest();
                }
                usuario.FlagAtivo = FlagAtivo;
                _context.Entry(usuario).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (!UsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw new Exception(e.Message);
                }
            }

            return NoContent();
        }

        [HttpPut("ValidarCodigo/{id}/{codValidacao}")]
        public async Task<bool> PutDesabilitaUsuario(int id, int codValidacao)
        {
            try
            {
                return await BLL.ValidarCodigo(id, codValidacao);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        private bool UsuarioExists(int id)
        {
            try
            {
                return _context.Usuario.Any(e => e.Id == id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
