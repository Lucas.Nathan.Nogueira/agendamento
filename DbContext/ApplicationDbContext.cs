﻿using Microsoft.EntityFrameworkCore;
using ApiGestao.Model;

namespace ApiGestao.Context
{
    public class ApplicationDbContext : DbContext
    {
        //CRIAR TABELA DE USUÁRIO
        public DbSet<Perfil> Perfil { get; set; }
        public DbSet<Endereco> Endereco { get; set; }
        public DbSet<Contato> Contato { get; set; }
        public DbSet<Pessoa> Pessoa { get; set; }
        public DbSet<Usuario> Usuario { get; set; }




        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        { }

    }

}
