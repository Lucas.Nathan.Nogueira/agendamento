﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGestao.Model
{
    public class Perfil
    {
        [Key]
        public int Id { get; set; }

        public string FlagIdentPerf { get; set; }

        public string Decricao { get; set; }

    }
}
