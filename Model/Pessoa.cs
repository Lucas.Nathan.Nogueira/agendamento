﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGestao.Model
{
    public class Pessoa
    {

        [Key]
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Cpf { get; set; }

        public string Rg { get; set; }

        [DataType(DataType.Date)]
        public DateTime DtNascimento { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DtCadastro { get; set; }



        [ForeignKey("contatoId")]
        public int contatoId { get; set; }
        public Contato contato { get; set; }
    }
}
