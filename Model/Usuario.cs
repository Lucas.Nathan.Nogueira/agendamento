﻿using ApiGestao.Model;
using NPOI.SS.Formula.Functions;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiGestao.Model
{
    public class Usuario
    {

        [Key]
        public int Id { get; set; }

        public string NomeUsr { get; set; }

        [DataType(DataType.Password)]
        public string Senha { get; set; }

        public bool FlagAtivo { get; set; }

        public int CodValidacao { get; set; }

        [ForeignKey("pessoaId")]
        public int PessoaId { get; set; }
        public Pessoa Pessoa { get; set; }
       
        [ForeignKey("perfilId")]
        public int PerfilId { get; set; }
        public Perfil Perfil { get; set; }

    }
}
