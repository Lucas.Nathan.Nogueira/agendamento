﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppGestao.ViewsModel
{
    public class Response<T>
    {
        public Response()
        {
            list = new List<T>();
            errors = new List<string>();
        }
        public T data { get; set; }
        public List<T> list { get; set; }
        public List<String> errors { get; set; }
        public Boolean success { get; set; }
        public String message { get; set; }
    }
}
