﻿using ApiGestao.Model;
using NPOI.SS.Formula.Functions;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiGestao.Model
{
    public class UsuarioView
    {

        public UsuarioView()
        {
            this.Perfil = new Perfil();
            this.Pessoa = new Pessoa();
        }
        [Key]
        public int Id { get; set; }

        public string NomeUsr { get; set; }

        public string access_token { get; set; }

        [DataType(DataType.Password)]
        public string Senha { get; set; }

        public bool FlagAtivo { get; set; }

        public int CodValidacao { get; set; }

        [ForeignKey("pessoaId")]
        public int PessoaId { get; set; }
        public Pessoa Pessoa { get; set; }
       
        [ForeignKey("perfilId")]
        public int PerfilId { get; set; }
        public Perfil Perfil { get; set; }

        public static implicit operator T(UsuarioView v)
        {
            throw new NotImplementedException();
        }
    }
}
